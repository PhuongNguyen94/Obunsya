//
//  NSObject+Interview.h
//  Obunsya
//
//  Created by DuyPhuong on 3/5/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StepContents:NSObject
+ (instancetype)shareInstance;
@property (nonatomic)NSString*desc;
@property (nonatomic)NSNumber*duration;
@property (nonatomic)NSString*duration_display;
@property (nonatomic)NSString*header_left;
@property (nonatomic)NSString*header_middle;
@property (nonatomic)NSString*header_right;
@property (nonatomic)NSString*image;
@property (nonatomic)NSNumber*jingle;
@property (nonatomic)NSString*media;
@property (nonatomic)NSNumber*record;
@property (nonatomic)NSString*text;
-(void)setdesc:(NSString*)desc;
-(NSString*)getdesc;
-(void)setduration:(NSNumber*)duration;
-(NSNumber*)getduration;
-(void)setduration_display:(NSString*)duration_display;
-(NSString*)getduration_display;
-(void)setheader_left:(NSString*)header_left;
-(NSString*)getheader_left;
-(void)setheader_middle:(NSString*)header_middle;
-(NSString*)getheader_middle;
-(void)setheader_right:(NSString*)header_right;
-(NSString*)getheader_right;
-(void)setimage:(NSString*)image;
-(NSString*)getimage;
-(void)setjingle:(NSNumber*)jingle;
-(NSNumber*)getjingle;
-(void)setmedia:(NSString*)media;
-(NSString*)getmedia;
-(void)setrecord:(NSNumber*)record;
-(NSNumber*)getrecord;
-(void)settext:(NSString*)text;
-(NSString*)gettext;




@end
