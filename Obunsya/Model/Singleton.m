//
//  NSObject+Singleton.m
//  Obunsya
//
//  Created by DuyPhuong on 3/1/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton : NSObject

+ (instancetype)shareInstance{
    

    static Singleton *_sharedInstance = nil;

    static dispatch_once_t oncePredicate;

    dispatch_once(&oncePredicate, ^{
        if (!_sharedInstance) {
            
            _sharedInstance = [[self alloc] init];
        }
    });
    return _sharedInstance;
}

-(void)settoken:(NSString *)token{
    self.Token = token;
}
-(NSString*)getToken{
    return self.Token;
}
-(void)setbooklet_id:(NSString *)booklet_id{
    self.Booklet_id = booklet_id;
}
-(NSString*)getBooklet_id{
    return self.Booklet_id;
}
-(void)setinterview:(NSArray *)interview{
    self.Interview = interview;
}
-(NSArray*)getInterview{
    return self.Interview ;
}

@end
