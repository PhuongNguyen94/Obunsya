//
//  NSObject+Singleton.h
//  Obunsya
//
//  Created by DuyPhuong on 3/1/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject
@property (nonatomic)NSString *Token;
@property (nonatomic)NSString *Booklet_id;
@property (nonatomic)NSArray *Interview;
+ (instancetype)shareInstance;
-(NSString*)getToken;
-(NSString*)getBooklet_id;
-(NSArray*)getInterview;
-(void)settoken:(NSString*)token;
-(void)setbooklet_id:(NSString*)booklet_id;
-(void)setinterview:(NSArray*)interview;
@end
