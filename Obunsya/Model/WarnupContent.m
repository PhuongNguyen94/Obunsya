//
//  NSObject+WarnupContent.m
//  Obunsya
//
//  Created by DuyPhuong on 3/5/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import "WarnupContent.h"

@implementation WarnupContent:NSObject
+ (instancetype)shareInstance{
    
    
    static WarnupContent *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        if (!_sharedInstance) {
            
            _sharedInstance = [[self alloc] init];
        }
    });
    return _sharedInstance;
}

-(void)setdesc:(NSArray *)desc{
    self.desc = desc;
}
-(NSArray*)getdesc{
    return self.desc;
}
-(void)setduration:(NSArray *)duration{
    self.duration = duration;
}
-(NSArray*)getduration{
    return self.duration;
}
-(void)setduration_display:(NSArray *)duration_display{
    self.duration_display = duration_display;
}
-(NSArray*)getduration_display{
    return self.duration_display;
}
-(void)setheader_left:(NSArray *)header_left{
    self.header_left = header_left;
}
-(NSArray*)getheader_left{
    return self.header_left;
}
-(void)setheader_right:(NSArray *)header_right{
    self.header_right = header_right;
}
-(NSArray*)getheader_right{
    return self.header_right;
}
-(void)setheader_middle:(NSArray *)header_middle{
    self.header_middle = header_middle;
}
-(NSArray*)getheader_middle{
    return self.header_middle;
}
-(void)setimage:(NSArray *)image{
    self.image = image;
}
-(NSArray*)getimage{
    return self.image;
}
-(void)setmedia:(NSArray *)media{
    self.media = media;
}
-(NSArray*)getmedia{
    return self.media;
}
-(void)setjingle:(NSArray *)jingle{
    self.jingle = jingle;
}
-(NSArray*)getjingle{
    return self.jingle;
}
-(void)setrecord:(NSArray *)record{
    self.record = record;
}
-(NSArray*)getrecord{
    return self.record;
}
-(void)settext:(NSArray *)text{
    self.text = text;
}
-(NSArray*)gettext{
    return self.gettext;
}
@end
