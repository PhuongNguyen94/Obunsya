//
//  NSObject+Interview.m
//  Obunsya
//
//  Created by DuyPhuong on 3/5/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import "StepContent.h"

@implementation StepContents:NSObject
+ (instancetype)shareInstance{
    
    
    static StepContents *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        if (!_sharedInstance) {
            
            _sharedInstance = [[self alloc] init];
        }
    });
    return _sharedInstance;
}

-(void)setdesc:(NSString *)desc{
    self.desc = desc;
}
-(NSString*)getdesc{
    return self.desc;
}
-(void)setduration:(NSNumber *)duration{
    self.duration = duration;
}
-(NSNumber*)getduration{
    return self.duration;
}
-(void)setduration_display:(NSString *)duration_display{
    self.duration_display = duration_display;
}
-(NSString*)getduration_display{
    return self.duration_display;
}
-(void)setheader_left:(NSString *)header_left{
    self.header_left = header_left;
}
-(NSString*)getheader_left{
    return self.header_left;
}
-(void)setheader_right:(NSString *)header_right{
    self.header_right = header_right;
}
-(NSString*)getheader_right{
    return self.header_right;
}
-(void)setheader_middle:(NSString *)header_middle{
    self.header_middle = header_middle;
}
-(NSString*)getheader_middle{
    return self.header_middle;
}
-(void)setimage:(NSString *)image{
    self.image = image;
}
-(NSString*)getimage{
    return self.image;
}
-(void)setmedia:(NSString *)media{
    self.media = media;
}
-(NSString*)getmedia{
    return self.media;
}
-(void)setjingle:(NSNumber *)jingle{
    self.jingle = jingle;
}
-(NSNumber*)getjingle{
    return self.jingle;
}
-(void)setrecord:(NSNumber *)record{
    self.record = record;
}
-(NSNumber*)getrecord{
    return self.record;
}
-(void)settext:(NSString *)text{
    self.text = text;
}
-(NSString*)gettext{
    return self.gettext;
}

@end
