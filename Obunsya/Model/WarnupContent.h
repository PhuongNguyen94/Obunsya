//
//  NSObject+WarnupContent.h
//  Obunsya
//
//  Created by DuyPhuong on 3/5/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WarnupContent:NSObject
+ (instancetype)shareInstance;
@property (nonatomic)NSArray*desc;
@property (nonatomic)NSArray*duration;
@property (nonatomic)NSArray*duration_display;
@property (nonatomic)NSArray*header_left;
@property (nonatomic)NSArray*header_middle;
@property (nonatomic)NSArray*header_right;
@property (nonatomic)NSArray*image;
@property (nonatomic)NSArray*jingle;
@property (nonatomic)NSArray*media;
@property (nonatomic)NSArray*record;
@property (nonatomic)NSArray*text;
-(void)setdesc:(NSArray*)desc;
-(NSArray*)getdesc;
-(void)setduration:(NSArray*)duration;
-(NSArray*)getduration;
-(void)setduration_display:(NSArray*)duration_display;
-(NSArray*)getduration_display;
-(void)setheader_left:(NSArray*)header_left;
-(NSArray*)getheader_left;
-(void)setheader_middle:(NSArray*)header_middle;
-(NSArray*)getheader_middle;
-(void)setheader_right:(NSArray*)header_right;
-(NSArray*)getheader_right;
-(void)setimage:(NSArray*)image;
-(NSArray*)getimage;
-(void)setjingle:(NSArray*)jingle;
-(NSArray*)getjingle;
-(void)setmedia:(NSArray*)media;
-(NSArray*)getmedia;
-(void)setrecord:(NSArray*)record;
-(NSArray*)getrecord;
-(void)settext:(NSArray*)text;
-(NSArray*)gettext;

@end
