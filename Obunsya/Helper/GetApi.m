//
//  NSObject+Getapi.m
//  Obunsya
//
//  Created by DuyPhuong on 3/1/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import "GetApi.h"
#import "Singleton.h"

@implementation GetApi : NSObject
//MARK :1 Get data with user_id
-(void)getDataLoginApi :(NSString *)user : (NSString *)pass{
    NSString *urlStr = [NSString stringWithFormat:@"http://spk1.jv-it.com.vn/sessions"];
    NSString *urlsString = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:urlsString];
    NSMutableURLRequest *urlrequest = [NSMutableURLRequest requestWithURL:url];
    [urlrequest setHTTPMethod:@"POST"];
    [urlrequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlrequest setValue:@"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25" forHTTPHeaderField:@"User-Agent"];
    NSString *postString = [NSString stringWithFormat:@"id=%@&password=%@&remember_me=false",user,pass];
    NSData* data = [postString dataUsingEncoding:NSUTF8StringEncoding];
    [urlrequest setHTTPBody:data];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *urlSection = [NSURLSession sessionWithConfiguration:config];
    NSURLSessionTask *task = [urlSection dataTaskWithRequest:urlrequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error){
            if(data){
                NSDictionary *mydict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                //MARK : - get toke and booked_id
                [[Singleton shareInstance]settoken:mydict[@"token"]];
                [[Singleton shareInstance]setbooklet_id:mydict[@"booklet_id"]];
            }
        }
    }];
    [task resume];
}
//MARK : - 2 request data
-(void)getData:(NSString*)token :(NSString*)URL :(NSString*)Booklet_id andFinish:(void(^)(void))finish {
    NSString *urlStr = [NSString stringWithFormat:@"http://spk1.jv-it.com.vn%@%@",URL,Booklet_id];
    NSString *urlsString = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:urlsString];
    NSMutableURLRequest *urlrequest = [NSMutableURLRequest requestWithURL:url];
    [urlrequest setHTTPMethod:@"GET"];
    [urlrequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlrequest setValue:@"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25" forHTTPHeaderField:@"User-Agent"];
    NSString *value = [NSString stringWithFormat:@"StudyGear token=%@",token];
    [urlrequest setValue:value forHTTPHeaderField:@"Authorization"];
    [urlrequest setValue:@"User-Agent" forHTTPHeaderField:@"User-Agent"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *urlSection = [NSURLSession sessionWithConfiguration:config];
    NSURLSessionTask *task = [urlSection dataTaskWithRequest:urlrequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error){
            if(data){
                NSDictionary *mydict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                NSArray *interviews = mydict[@"interviews"];
                NSMutableArray *myarray = [NSMutableArray array];
                for(NSDictionary*interview in interviews){
                    [myarray addObject:interview[@"interview_id"]];
                }
                //MARK : - get interview_id
                [[Singleton shareInstance]setinterview:myarray];
                finish();
            }   
        }
    }];
    [task resume];
}
//MARK : 6  Creaet interview result API
-(void)getInterviewResults{
    NSArray *interview_id = [[Singleton shareInstance]getInterview];
    for(NSString*interview in interview_id){
        NSString *urlStr = [NSString stringWithFormat:@"http://spk1.jv-it.com.vn/users/apptest_40/interview-results"];
        NSString *urlsString = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlsString];
        NSMutableURLRequest *urlrequest = [NSMutableURLRequest requestWithURL:url];
        [urlrequest setHTTPMethod:@"POST"];
        [urlrequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [urlrequest setValue:@"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25" forHTTPHeaderField:@"User-Agent"];
        NSString *token = [[Singleton shareInstance]getToken];
        NSString *value = [NSString stringWithFormat:@"StudyGear token=%@",token];
        [urlrequest setValue:value forHTTPHeaderField:@"Authorization"];
        NSString *postString = [NSString stringWithFormat:@"interview_id=%@",interview];
        NSData* data = [postString dataUsingEncoding:NSUTF8StringEncoding];
        [urlrequest setHTTPBody:data];
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *urlSection = [NSURLSession sessionWithConfiguration:config];
        NSURLSessionTask *task = [urlSection dataTaskWithRequest:urlrequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if(!error){
                if(data){
                    NSDictionary *mydict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    NSLog(@"%@",mydict);
                }
            }
        }];
        [task resume];
    }
}
//MARK : -2 Get data all steps contents

-(void)getAllsteps :(NSString*)urlbase :(void(^)(NSDictionary*mydict))finish{
    NSString *urlStr = [NSString stringWithFormat:@"http://spk1.jv-it.com.vn%@",urlbase];
    NSString *urlsString = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:urlsString];
    NSMutableURLRequest *urlrequest = [NSMutableURLRequest requestWithURL:url];
    [urlrequest setHTTPMethod:@"GET"];
    [urlrequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlrequest setValue:@"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25" forHTTPHeaderField:@"User-Agent"];
    NSString *token = [[Singleton shareInstance]getToken];
    NSString *value = [NSString stringWithFormat:@"StudyGear token=%@",token];
    [urlrequest setValue:value forHTTPHeaderField:@"Authorization"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *urlSection = [NSURLSession sessionWithConfiguration:config];
    NSURLSessionTask *task = [urlSection dataTaskWithRequest:urlrequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error){
            if(data){
                NSDictionary *mydict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                finish(mydict);
                
            }
        }
    }];
    [task resume];
}
//MARK : - 5 Get audio
-(void)getVideoAudioFromServer:(void(^)(NSDictionary*mydict))finish;{
    NSString *urlStr = [NSString stringWithFormat:@"http://spk1.jv-it.com.vn/SpeakingTest/getVideoAudioFromServer"];
    NSString *urlsString = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:urlsString];
    NSMutableURLRequest *urlrequest = [NSMutableURLRequest requestWithURL:url];
    [urlrequest setHTTPMethod:@"POST"];
    [urlrequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlrequest setValue:@"Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25" forHTTPHeaderField:@"User-Agent"];
    NSString *booklet_id = [[Singleton shareInstance]getBooklet_id];
    NSString *postString = [NSString stringWithFormat:@"booklet_id=%@",booklet_id];
    NSString *token = [[Singleton shareInstance]getToken];
    NSString *value = [NSString stringWithFormat:@"StudyGear token=%@",token];
    [urlrequest setValue:value forHTTPHeaderField:@"Authorization"];
    NSData* data = [postString dataUsingEncoding:NSUTF8StringEncoding];
    [urlrequest setHTTPBody:data];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *urlSection = [NSURLSession sessionWithConfiguration:config];
    NSURLSessionTask *task = [urlSection dataTaskWithRequest:urlrequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error){
            if(data){
                NSDictionary *mydict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                finish(mydict);
            }
        }
    }];
    [task resume];
}

//MARK : - Download with url
- (void)downloadFileFromUrl:(NSString *)urlStr{
    NSArray *fileNameArr = [urlStr componentsSeparatedByString:@"/"];
    NSString *fileName = fileNameArr.lastObject;
    
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentDir stringByAppendingPathComponent:fileName];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (error) {
            NSLog(@"Download Error:%@",error.description);
        }
        if (data) {
            [data writeToFile:filePath atomically:YES];
            NSLog(@"File is saved to %@",filePath);
        }
    }];
}

@end
