//
//  NSObject+Getapi.h
//  Obunsya
//
//  Created by DuyPhuong on 3/1/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetApi :NSObject
-(void)getDataLoginApi :(NSString *)user : (NSString *)pass;
-(void)getData:(NSString*)token :(NSString*)URL :(NSString*)Booklet_id andFinish:(void(^)(void))finish;
-(void)getInterviewResults;
-(void)getVideoAudioFromServer:(void(^)(NSDictionary*mydict))finish;
-(void)downloadFileFromUrl:(NSString *)urlStr;
-(void)getAllsteps :(NSString*)urlbase :(void(^)(NSDictionary*mydict))finish;
@end
