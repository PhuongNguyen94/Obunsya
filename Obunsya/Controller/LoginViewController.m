//
//  LoginViewController.m
//  Obunsya
//
//  Created by DuyPhuong on 2/27/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import "LoginViewController.h"
#import "DownloadViewController.h"
#import "Singleton.h"
@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize User;
- (void)viewDidLoad {
    [super viewDidLoad];

    self->Get_All_Interviews = @"/interviews?booklet_id=";
    self->GET_Interview = @"/interviews/";
    self.txt_Id.text = @"apptest_40";
    self.txt_Pass.text = @"11111111";
    
}
-(void)viewWillAppear:(BOOL)animated{
    //MARK : - Hide login button
    self.bnt_LoginView.hidden = YES;
    //MARK : - Show alert
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"ご注意"
                                 message:@" 英語応対能力検定の「スピーキングテスト」専用のアプリです。まず、「リーディング・リスニングテスト」を受験された後に、このアプリで「スピーキングテスト」を​受験​してください。"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"続ける"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                   self.bnt_LoginView.hidden = NO;
                                }];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    User = [[GetApi alloc]init];
    [User getDataLoginApi:self.txt_Id.text :self.txt_Pass.text];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bnt_Login:(id)sender {
    NSString*token = [[Singleton shareInstance]getToken];
    NSString*booklet_id = [[Singleton shareInstance]getBooklet_id];
    [User getData:token :self->Get_All_Interviews :booklet_id andFinish:^{
        dispatch_async(dispatch_get_main_queue(), ^{
             [self showDownloadViewController];
        });
        
    }];
}
//MARK : Show view download
-(void)showDownloadViewController{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DownloadViewController *vc = (DownloadViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"downloadView"];
    [self presentViewController:vc animated:YES completion:^{
    }];
}

@end
