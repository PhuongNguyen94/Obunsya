//
//  ViewController.m
//  Obunsya
//
//  Created by DuyPhuong on 2/27/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import "HomeController.h"
#import "LoginViewController.h"

@interface HomeController ()

@end

@implementation HomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bnt_Next:(id)sender {
     [self performSelector:@selector(showLoginController) withObject:nil afterDelay:0.01];
}
//MARK : show view login
-(void)showLoginController{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginViewController *vc = (LoginViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"loginView"];
    [self presentViewController:vc animated:YES completion:^{

    }];
}



@end
