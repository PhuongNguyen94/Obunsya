//
//  DownloadViewController.m
//  Obunsya
//
//  Created by DuyPhuong on 2/27/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import "DownloadViewController.h"
#import "Singleton.h"
#import "GetApi.h"
#import "StepContent.h"
#import "WarnupContent.h"


@interface DownloadViewController ()

@end

@implementation DownloadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self->GET_ALL_TEST_CONTENT = @"/SpeakingTest/getAllStepTestContents";
    self->GET_ALL_STEP_WARM_UP  = @"/SpeakingTest/getAllStepWarmUp";
    GetApi *getapi = [[GetApi alloc]init];
   // [getapi getInterviewResults];
        [getapi getVideoAudioFromServer:^(NSDictionary *mydict) {
            NSArray*interview = mydict[@"interviews"];
            NSDictionary *testContents = mydict[@"testContents"];
            NSDictionary *warmUp = mydict[@"warmup"];
            NSArray*warmup = [warmUp allValues];
            NSArray *testcontents = [testContents allValues];
//            for (NSString* i in interview){
//                NSString *url = [NSString stringWithFormat:@"http://spk1.jv-it.com.vn%@",i];
//                [getapi downloadFileFromUrl:url];
//            }
//            for (NSString* i in warmup){
//                NSString *url = [NSString stringWithFormat:@"http://spk1.jv-it.com.vn%@",i];
//                [getapi downloadFileFromUrl:url];
//            }
            for (NSString* i in testcontents){
                NSString *url = [NSString stringWithFormat:@"http://spk1.jv-it.com.vn%@",i];
                [getapi downloadFileFromUrl:url];
            }
        }];
    [getapi getAllsteps:self->GET_ALL_TEST_CONTENT :^(NSDictionary *mydict) {
        NSLog(@"%@",mydict);
        for(NSDictionary*i in mydict){
            [[StepContents shareInstance]setdesc:i[@"desc"]];
            [[StepContents shareInstance]setduration:i[@"duration"]];
            [[StepContents shareInstance]setduration_display:i[@"duration_display"]];
            [[StepContents shareInstance]setheader_left:i[@"header_left"]];
            [[StepContents shareInstance]setheader_right:i[@"header_right"]];
            [[StepContents shareInstance]setheader_middle:i[@"header_middle"]];
            [[StepContents shareInstance]setimage:i[@"image"]];
            [[StepContents shareInstance]setmedia:i[@"media"]];
            [[StepContents shareInstance]setjingle:i[@"jingle"]];
            [[StepContents shareInstance]settext:i[@"text"]];
            [[StepContents shareInstance]setrecord:i[@"record"]];
        }
    }];
    [getapi getAllsteps:self->GET_ALL_STEP_WARM_UP :^(NSDictionary *mydict) {
        NSArray *record = [mydict valueForKey:@"record"];
        [[WarnupContent shareInstance]setrecord:record];
        NSArray *desc = [mydict valueForKey:@"desc"];
        [[WarnupContent shareInstance]setdesc:desc];
        NSArray *duration = [mydict valueForKey:@"duration"];
        [[WarnupContent shareInstance]setduration:duration];
        NSArray *duration_display = [mydict valueForKey:@"duration_display"];
        [[WarnupContent shareInstance]setduration_display:duration_display];
        NSArray *header_left = [mydict valueForKey:@"header_left"];
        [[WarnupContent shareInstance]setheader_left:header_left];
        NSArray *header_right = [mydict valueForKey:@"header_right"];
        [[WarnupContent shareInstance]setheader_right:header_right];
        NSArray *header_middle = [mydict valueForKey:@"header_middle"];
        [[WarnupContent shareInstance]setheader_middle:header_middle];
        NSArray *image = [mydict valueForKey:@"image"];
        [[WarnupContent shareInstance]setimage:image];
        NSArray *media = [mydict valueForKey:@"media"];
        [[WarnupContent shareInstance]setmedia:media];
        NSArray *jingle = [mydict valueForKey:@"jingle"];
        [[WarnupContent shareInstance]setjingle:jingle];
        NSArray *text = [mydict valueForKey:@"text"];
        [[WarnupContent shareInstance]settext:text];
    }];
}
- (IBAction)bnt_next:(id)sender {
    
}
@end
