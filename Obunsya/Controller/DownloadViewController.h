//
//  DownloadViewController.h
//  Obunsya
//
//  Created by DuyPhuong on 2/27/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadViewController : UIViewController{
    NSString *GET_ALL_TEST_CONTENT;
    NSString *GET_ALL_STEP_WARM_UP;
   
}
@property (weak, nonatomic) IBOutlet UIProgressView *pgv_DownloadView;

@end
