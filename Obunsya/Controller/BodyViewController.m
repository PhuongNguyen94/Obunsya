//
//  BodyViewController.m
//  Obunsya
//
//  Created by DuyPhuong on 3/5/18.
//  Copyright © 2018 DuyPhuong. All rights reserved.
//

#import "BodyViewController.h"
#import "GetApi.h"
#import "WarnupContent.h"
#import "StepContent.h"

@interface BodyViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image_view;

@end

@implementation BodyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bnt_play:(id)sender {
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentDir stringByAppendingPathComponent:@"201703_testcontents_app.jpg"];
    NSString *filePath1 = [documentDir stringByAppendingString:@"sound_test.mp3"];
    _image_view.image = [UIImage imageWithContentsOfFile:filePath];
}
- (IBAction)bnt_next:(id)sender {

}
@end
